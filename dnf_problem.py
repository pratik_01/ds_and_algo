# Problem 3

# Code:

def sort_array(array):
    for i in range(0, len(array)):
        for j in range(i+1):
            if array[i] < array[j]:
                array[i], array[j] = array[j], array[i]
    print(array)


if '__main__' == __name__:
    array = [0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1]
    sort_array(array)
