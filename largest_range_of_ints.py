from datetime import datetime
# Problem 5

# Code:


def array_of_int(array):
    start = datetime.now()
    aux =[]
    for i in range(len(array)):
        for j in range(i+1):
            if array[i] == array[j] - 1:
                aux.append(array[i])
                aux.append(array[j])

    end = datetime.now()
    print("Total Execution time:", end-start)
    return aux


if '__main__' == __name__:
    array = [1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6]
    print(array_of_int(array))


