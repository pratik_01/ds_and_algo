import heapq


class Plates:

    def __init__(self, capacity):
        self.stack = []
        self.capacity = capacity
        self.original = []
        self.second = []

    def push(self, value):
        if self.original:
            idx = self.original[0]
            self.stack[idx] += [value]
            if len(self.stack[idx]) == self.capacity:
                heapq.heappop(self.original)
        else:
            idx = len(self.stack)
            heapq.heappush(self.original, idx)
            heapq.heappush(self.second, -idx)
            self.stack += [[value]]

    def pop(self):
        while self.original and not self.stack[-self.second[0]]:
            heapq.heappop(self.second)
        if self.second:
            idx = -self.second[0]
            return self.pop_at_stack(idx)
        return -1

    def pop_at_stack(self, idx):
        if self.stack[idx]:
            if len(self.stack[idx]) == self.capacity:
                heapq.heappush(self.original, idx)
            return self.stack[idx].pop()
        return -1




