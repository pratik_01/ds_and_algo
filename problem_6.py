def equal_products(array):
    the_len = len(array)
    left = [0]*the_len
    right = [0]*the_len
    final = [0]*the_len

    left[0] = 1

    right[the_len-1] = 1

    for i in range(1, the_len):
        left[i] = array[i-1] * left[i-1]
    for j in range(the_len-2, -1, -1):
        right[j] = array[j+1] * right[j+1]
    for i in range(the_len):
        final[i] = left[i] * right[i]

    return final


if'__main__' == __name__:
    array = [5, 1, 4, 2]
    print(equal_products(array))



