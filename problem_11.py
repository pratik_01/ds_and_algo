class Node:
    def __init__(self, key):
        self.data = key
        self.left = self.right = None


def newNode(data):
    temp = Node(0)
    temp.data = data
    temp.left = temp.right = None
    return temp


def printArr(arr, n):
    i = 0
    while (i < n):
        print(arr[i], end=",")
        i = i + 1


def getHeight(root):
    if root.left is None and root.right is None:
        return 0

    left = 0
    if (root.left != None):
        left = getHeight(root.left)

    right = 0
    if (root.right != None):
        right = getHeight(root.right)

    return (max(left, right) + 1)


#summ = []


def calculateLevelSum(node, level):
    global summ
    if (node == None):
        return

    # Add current node data to the sum
    # of the current node's level
    summ[level] += node.data

    # Recursive call for left and right sub-tree
    calculateLevelSum(node.left, level + 1)
    calculateLevelSum(node.right, level + 1)

root = newNode(1)
root.left = newNode(2)
root.right = newNode(3)
root.left.left = newNode(3)
root.left.right = newNode(4)
root.right.left = newNode(5)
root.right.right = newNode(6)
root.left.left.right = newNode(7)
root.right.right.left = newNode(8)

levels = getHeight(root) + 1

summ = [0] * levels
calculateLevelSum(root, 0)

printArr(summ, levels)
