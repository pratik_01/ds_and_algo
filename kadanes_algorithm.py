# Problem 1

# Code:

def max_sum(array):
    current = array[0]
    maximum = array[0]

    for x in range(1, len(array)):
        current = max(array[x], current+array[x])
        maximum = max(maximum, current)
    return maximum


if '__main__' == __name__:

    array = [-2, -3, 4, -1, -2, 1, 5, -3]
    the_max_sum = max_sum(array)
    print(the_max_sum)